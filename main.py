from tkinter import *


def button_clicked():
    km = round(float(qty_miles.get())*1.609344)
    result_lb.config(text=f"{km}")


window = Tk()
window.title('Convert Miles to Km')
window.minsize(width=300,height=100)
window.config(padx=30,pady=30)

# Entry
qty_miles = Entry(width=10,font=("Arial", 14, "bold"))
qty_miles.grid(row=0,column=1)

# label Miles
lb1 = Label(text='Miles',font=("Arial", 14, "bold"))
lb1.grid(row=0,column=2)


# label
lb2 = Label(text='Is equal to',font=("Arial", 14, "bold"))
lb2.grid(row=1,column=0)

result_lb = Label(text='0',font=("Arial", 12, "italic"))
result_lb.grid(row=1,column=1)

# label
lb4 = Label(text="Km",font=("Arial", 14, "bold"),)
lb4.grid(row=1,column=2)


# button
button = Button(text='Calculate',font=("Arial", 14, "bold"), command=button_clicked)
button.grid(row=2,column=1)


window.mainloop()

